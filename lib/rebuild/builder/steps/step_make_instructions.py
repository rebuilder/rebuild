#!/usr/bin/env python
#-*- coding:utf-8; mode:python; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-

import os, os.path as path

from bes.common.variable import variable
from bes.fs.file_util import file_util
from rebuild.step.step import step
from rebuild.step.step_result import step_result

class step_make_instructions(step):
  'Make and save build instructions for packages.'

  def __init__(self):
    super(step_make_instructions, self).__init__()

  #@abstractmethod
  @classmethod
  def define_args(clazz):
    return ''
    
  #@abstractmethod
  def execute(self, script, env, values, inputs):
    if not script.instructions:
      message = 'No build instructions for %s' % (script.descriptor.full_name)
      self.log_d(message)
      return step_result(True, message)
    success, message = script.instructions.save(script.staged_files_instructions_dir)
    if not success:
      self.log_d(message)
      return step_result(False, message)
    return step_result(True, None)

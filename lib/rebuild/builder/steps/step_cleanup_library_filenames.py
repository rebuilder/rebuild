#!/usr/bin/env python
#-*- coding:utf-8; mode:python; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-

import os.path as path

from bes.fs.file_symlink import file_symlink
from bes.common.string_util import string_util
from rebuild.toolchain.library import library
from rebuild.step.step import step
from rebuild.step.step_result import step_result
from rebuild.pkg_config.pkg_config import pkg_config

class step_cleanup_library_filenames(step):
  'Cleanups realted to the filenames of libraries.'

  def __init__(self):
    super(step_cleanup_library_filenames, self).__init__()

  @classmethod
  def define_args(clazz):
    return '''
    skip_binary_third_party_prefix   bool        False
    '''
    
  #@abstractmethod
  def execute(self, script, env, values, inputs):
    if values.get('skip_binary_third_party_prefix'):
      return step_result(True, None)
    if path.isdir(script.staged_files_lib_dir):
      libraries = library.list_libraries(script.staged_files_lib_dir, relative = True)
      for l in libraries:
        link_filename = library.name_add_prefix(l, env.config.third_party_prefix)
        link_path = path.join(script.staged_files_lib_dir, link_filename)
        file_symlink.symlink(l, link_path)

    pc_files = pkg_config.find_pc_files(script.staged_files_dir)
    for pc_file in pc_files:
      pc_file_basename = path.basename(pc_file)
      new_pc_file = self._pc_file_add_third_party_prefix(script, env, pc_file)
      file_symlink.symlink(pc_file_basename, new_pc_file)
    return step_result(True, None)

  @classmethod
  def _pc_file_add_third_party_prefix(clazz, script, env, filename):
    basename = path.basename(filename)
    if basename.startswith('lib'):
      new_base = 'lib%s%s' % (env.config.third_party_prefix, string_util.remove_head(basename, 'lib'))
    else:
      new_base = '%s%s' % (env.config.third_party_prefix, basename)
    return path.join(path.dirname(filename), new_base)

#-*- coding:utf-8; mode:python; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-

import json, os.path as path, hashlib
from collections import namedtuple
from bes.common.check import check
from bes.common.json_util import json_util
from bes.common.object_util import object_util
from bes.common.type_checked_list import type_checked_list
from bes.compat.StringIO import StringIO

from .package_file import package_file
  
class package_file_list(type_checked_list):

  __value_type__ = package_file
  
  def __init__(self, values = None):
    super(package_file_list, self).__init__(values = values)

  @classmethod
  def cast_value(clazz, entry):
    if isinstance(entry, ( tuple, list )):
      return package_file(*entry)
    return entry
    
  def to_json(self):
    return json_util.to_json(self._values, indent = 2)
    
  def to_simple_list(self):
    return [ x.to_list() for x in self ]
    
  @classmethod
  def from_json(clazz, text):
    o = json.loads(text)
    check.check_list(o)
    return clazz.from_simple_list(o)
    
  # needed
  @classmethod
  def from_simple_list(clazz, l):
    check.check_list(l)
    result = clazz()
    for item in l:
      check.check_list(item)
      assert len(item) == 3
      check.check_string(item[0])
      check.check_string(item[1])
      check.check_bool(item[2])
      result.append(package_file(item[0], item[1], item[2]))
    return result

  # needed
  @classmethod
  def from_files(clazz, filenames, files_with_hardcoded_paths, root_dir = None, function_name = None):
    filenames = object_util.listify(filenames)
    files_with_hardcoded_paths = files_with_hardcoded_paths or set()
    result = clazz()
    for filename in filenames:
      has_hardcoded_path = filename in files_with_hardcoded_paths
      result.append(package_file.from_file(filename, has_hardcoded_path, root_dir = root_dir, function_name = function_name))
    return result

  def filenames(self):
    return [ c.filename for c in self ]

  def checksums(self):
    result = {}
    for entry in self:
      result[entry.filename] = entry.checksum
    return result
  
  def files_with_hardcoded_paths(self):
    return [ c.filename for c in self if c.has_hardcoded_path ]

  def checksum(self):
    'Return a checksum of the file list.'
    buf = StringIO()
    for value in self:
      buf.write(str(value))
    return hashlib.sha256(buf.getvalue().encode('utf-8')).hexdigest()

  def Xto_dict(self):
    'Return a dictionary of filenames to checksums.'
    result = {}
    for value in self:
      result[value.filename] = value.checksum
    return result

check.register_class(package_file_list, include_seq = False)

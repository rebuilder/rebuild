#!/usr/bin/env python
#-*- coding:utf-8; mode:python; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-

from rebuild.storage.sources_cli import sources_cli

if __name__ == '__main__':
  sources_cli.run()

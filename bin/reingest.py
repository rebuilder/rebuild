#!/usr/bin/env python
#-*- coding:utf-8; mode:python; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-

from rebuild.ingest.ingest_cli import ingest_cli

if __name__ == '__main__':
  ingest_cli.run()

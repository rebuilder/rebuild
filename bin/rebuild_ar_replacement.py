#!/usr/bin/env python
#-*- coding:utf-8; mode:python; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-

from rebuild.toolchain.ar_replacement_cli import ar_replacement_cli

if __name__ == '__main__':
  ar_replacement_cli.run()
